package accounts

import (
	"../../helper"
	"../../models"
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"net/http"
)

func GetAccounts(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")
	var accounts []models.Account

	collection := helper.SelectCollection("accounts")

	cur, err := collection.Find(context.TODO(), bson.M{})
	if err != nil{
		helper.GetError(err, w)
		return
	}

	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()){
		var account models.Account
		err := cur.Decode(&account)
		if err != nil{
			log.Fatal(err)
		}

		accounts = append(accounts, account)
	}

	if err := cur.Err(); err != nil{
		log.Fatal(err)
	}

	json.NewEncoder(w).Encode(accounts)
}

func GetAccount(w http.ResponseWriter, r *http.Request) {
	// set header.
	w.Header().Set("Content-Type", "application/json")

	var account models.Account
	var params = mux.Vars(r)

	id, _ := primitive.ObjectIDFromHex(params["id"])

	collection := helper.SelectCollection("accounts")

	filter := bson.M{"_id": id}
	err := collection.FindOne(context.TODO(), filter).Decode(&account)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	json.NewEncoder(w).Encode(account)
}

func CreateAccount(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var account models.Account

	_ = json.NewDecoder(r.Body).Decode(&account)

	collection := helper.SelectCollection("accounts")

	result, err := collection.InsertOne(context.TODO(), account)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	json.NewEncoder(w).Encode(result)
}

func UpdateAccount(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var params = mux.Vars(r)

	//Get id from parameters
	id, _ := primitive.ObjectIDFromHex(params["id"])

	var account models.Account

	collection := helper.SelectCollection("accounts")

	// Create filter
	filter := bson.M{"_id": id}

	// Read update model from body request
	_ = json.NewDecoder(r.Body).Decode(&account)

	// prepare update model.
	update := bson.D{
		{"$set", bson.D{
			{"email", account.Email},
			{"password", account.Password},
			{"rol", account.Rol},
			{"prUi", account.PrUI},
		}},
	}
	err := collection.FindOneAndUpdate(context.TODO(), filter, update).Decode(&account)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	account.ID = id

	json.NewEncoder(w).Encode(account)
}

func DeleteAccount(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var params = mux.Vars(r)
	id, err := primitive.ObjectIDFromHex(params["id"])

	collection := helper.SelectCollection("accounts")

	filter := bson.M{"_id": id}

	deleteResult, err := collection.DeleteOne(context.TODO(), filter)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	json.NewEncoder(w).Encode(deleteResult)
}
