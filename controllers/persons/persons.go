package persons

import (
	"../../helper"
	"../../models"
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"net/http"
)

func GetPersons(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")
	var persons []models.Person

	collection := helper.SelectCollection("persons")

	cur, err := collection.Find(context.TODO(), bson.M{})
	if err != nil{
		helper.GetError(err, w)
		return
	}

	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()){
		var person models.Person
		err := cur.Decode(&person)
		if err != nil{
			log.Fatal(err)
		}
		persons = append(persons, person)
	}

	if err := cur.Err(); err != nil{
		log.Fatal(err)
	}

	json.NewEncoder(w).Encode(persons)
}

func GetPerson(w http.ResponseWriter, r *http.Request) {
	// set header.
	w.Header().Set("Content-Type", "application/json")

	var person models.Person
	var params = mux.Vars(r)

	id, _ := primitive.ObjectIDFromHex(params["id"])

	collection := helper.SelectCollection("persons")

	filter := bson.M{"_id": id}
	err := collection.FindOne(context.TODO(), filter).Decode(&person)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	json.NewEncoder(w).Encode(person)
}

func CreatePerson(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var person models.Person

	_ = json.NewDecoder(r.Body).Decode(&person)

	collection := helper.SelectCollection("persons")

	result, err := collection.InsertOne(context.TODO(), person)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	json.NewEncoder(w).Encode(result)
}

func UpdatePerson(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var params = mux.Vars(r)

	//Get id from parameters
	id, _ := primitive.ObjectIDFromHex(params["id"])

	var person models.Person

	collection := helper.SelectCollection("persons")

	// Create filter
	filter := bson.M{"_id": id}

	// Read update model from body request
	_ = json.NewDecoder(r.Body).Decode(&person)

	// prepare update model.
	update := bson.D{
		{"$set", bson.D{
			{"firstname", person.FirstName},
			{"lastname", person.LastName},
			{"avatar", person.Avatar},
			{"location", person.Location},
		}},
	}
	err := collection.FindOneAndUpdate(context.TODO(), filter, update).Decode(&person)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	person.ID = id

	json.NewEncoder(w).Encode(person)
}

func DeletePerson(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var params = mux.Vars(r)
	id, err := primitive.ObjectIDFromHex(params["id"])

	collection := helper.SelectCollection("persons")

	filter := bson.M{"_id": id}

	deleteResult, err := collection.DeleteOne(context.TODO(), filter)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	json.NewEncoder(w).Encode(deleteResult)
}
