package travels

import (
	"../../helper"
	"../../models"
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"net/http"
)

func GetTravels(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")
	var travels []models.Travel

	collection := helper.SelectCollection("travels")

	cur, err := collection.Find(context.TODO(), bson.M{})
	if err != nil{
		helper.GetError(err, w)
		return
	}

	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()){
		var travel models.Travel
		err := cur.Decode(&travel)
		if err != nil{
			log.Fatal(err)
		}
		travels = append(travels, travel)
	}

	if err := cur.Err(); err != nil{
		log.Fatal(err)
	}

	json.NewEncoder(w).Encode(travels)
}

func GetTravel(w http.ResponseWriter, r *http.Request) {
	// set header.
	w.Header().Set("Content-Type", "application/json")

	var travel models.Travel
	var params = mux.Vars(r)

	id, _ := primitive.ObjectIDFromHex(params["id"])

	collection := helper.SelectCollection("travels")

	filter := bson.M{"_id": id}
	err := collection.FindOne(context.TODO(), filter).Decode(&travel)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	json.NewEncoder(w).Encode(travel)
}

func CreateTravel(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var travel models.Travel

	_ = json.NewDecoder(r.Body).Decode(&travel)

	collection := helper.SelectCollection("travels")

	result, err := collection.InsertOne(context.TODO(), travel)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	json.NewEncoder(w).Encode(result)
}

func UpdateTravel(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var params = mux.Vars(r)

	//Get id from parameters
	id, _ := primitive.ObjectIDFromHex(params["id"])

	var travel models.Travel

	collection := helper.SelectCollection("travels")

	// Create filter
	filter := bson.M{"_id": id}

	// Read update model from body request
	_ = json.NewDecoder(r.Body).Decode(&travel)

	// prepare update model.
	update := bson.D{
		{"$set", bson.D{
			{"vehicleId", travel.VehicleID},
			{"accountId", travel.AccountID},
			{"startLocation", travel.StartLocation},
			{"endLocation", travel.EndLocation},
			{"status", travel.Status},
			{"updated", primitive.Timestamp{}},
		}},
	}
	err := collection.FindOneAndUpdate(context.TODO(), filter, update).Decode(&travel)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	travel.ID = id

	json.NewEncoder(w).Encode(travel)
}

func DeleteTravel(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var params = mux.Vars(r)
	id, err := primitive.ObjectIDFromHex(params["id"])

	collection := helper.SelectCollection("travels")

	filter := bson.M{"_id": id}

	deleteResult, err := collection.DeleteOne(context.TODO(), filter)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	json.NewEncoder(w).Encode(deleteResult)
}
