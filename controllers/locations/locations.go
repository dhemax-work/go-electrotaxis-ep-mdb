package locations

import (
	"../../helper"
	"../../models"
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"net/http"
)

var CollectionName = "locations"

func GetLocations(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")

	var locations []models.Location

	collection := helper.SelectCollection(CollectionName)
	cur, err := collection.Find(context.TODO(), bson.M{})

	if err != nil{
		helper.GetError(err, w)
		return
	}

	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()){
		var location models.Location
		err := cur.Decode(err)
		if err != nil{
			log.Fatal(err)
		}

		locations = append(locations, location)
	}

	json.NewEncoder(w).Encode(locations)
}

func GetLocation(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")
	var location models.Location
	var params = mux.Vars(r)

	id, _ := primitive.ObjectIDFromHex(params["id"])
	collection := helper.SelectCollection(CollectionName)

	filter := bson.M{"_id": id}
	err := collection.FindOne(context.TODO(), filter).Decode(&location)

	if err != nil{
		helper.GetError(err, w)
		return
	}
	json.NewEncoder(w).Encode(location)
}

func CreateLocation(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")

	var location models.Location

	_ = json.NewDecoder(r.Body).Decode(&location)

	collection := helper.SelectCollection(CollectionName)

	result, err := collection.InsertOne(context.TODO(), location)

	if err != nil {
		helper.GetError(err, w)
		return
	}
	json.NewEncoder(w).Encode(result)
}

func UpdateLocation(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")

	var params = mux.Vars(r)

	id, _ := primitive.ObjectIDFromHex(params["id"])
	var location models.Location

	collection := helper.SelectCollection(CollectionName)
	filter := bson.M{"_id": id}

	_ = json.NewDecoder(r.Body).Decode(&location)

	update := bson.D{
		{
			"$set", bson.D{
				{"xposition", location.XPosition},
				{"zposition", location.ZPosition},
				{"updated", location.Timestamp},
		}},
	}

	err := collection.FindOneAndUpdate(context.TODO(), filter, update).Decode(&location)
	if err != nil{
		helper.GetError(err, w)
		return
	}

	location.ID = id
	json.NewEncoder(w).Encode(location)
}

func DeleteLocation(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")

	var params = mux.Vars(r)
	id, err := primitive.ObjectIDFromHex(params["id"])
	collection := helper.SelectCollection(CollectionName)
	filter := bson.M{"_id": id}

	deleteResult, err := collection.DeleteOne(context.TODO(), filter)

	if err != nil {
		helper.GetError(err, w)
	}
	json.NewEncoder(w).Encode(deleteResult)
}