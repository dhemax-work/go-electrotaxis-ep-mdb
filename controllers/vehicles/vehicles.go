package vehicles

import (
	"../../helper"
	"../../models"
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"net/http"
)

func GetVehicles(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")
	var vehicles []models.Vehicle

	collection := helper.SelectCollection("vehicles")

	cur, err := collection.Find(context.TODO(), bson.M{})
	if err != nil{
		helper.GetError(err, w)
		return
	}

	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()){
		var vehicle models.Vehicle
		err := cur.Decode(&vehicle)
		if err != nil{
			log.Fatal(err)
		}

		vehicles = append(vehicles, vehicle)
	}

	if err := cur.Err(); err != nil{
		log.Fatal(err)
	}

	json.NewEncoder(w).Encode(vehicles)
}

func GetVehicle(w http.ResponseWriter, r *http.Request) {
	// set header.
	w.Header().Set("Content-Type", "application/json")

	var vehicle models.Vehicle
	var params = mux.Vars(r)

	id, _ := primitive.ObjectIDFromHex(params["id"])

	collection := helper.SelectCollection("vehicles")

	filter := bson.M{"_id": id}
	err := collection.FindOne(context.TODO(), filter).Decode(&vehicle)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	json.NewEncoder(w).Encode(vehicle)
}

func CreateVehicle(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var vehicle models.Account

	_ = json.NewDecoder(r.Body).Decode(&vehicle)

	collection := helper.SelectCollection("vehicles")

	result, err := collection.InsertOne(context.TODO(), vehicle)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	json.NewEncoder(w).Encode(result)
}

func UpdateVehicle(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var params = mux.Vars(r)

	//Get id from parameters
	id, _ := primitive.ObjectIDFromHex(params["id"])

	var vehicle models.Vehicle

	collection := helper.SelectCollection("vehicles")

	// Create filter
	filter := bson.M{"_id": id}

	// Read update model from body request
	_ = json.NewDecoder(r.Body).Decode(&vehicle)

	// prepare update model.
	update := bson.D{
		{"$set", bson.D{
			{"patent", vehicle.Patent},
			{"location", vehicle.Location},
			{"accountId", vehicle.AccountID},
			{"updated", primitive.Timestamp{}},
		}},
	}
	err := collection.FindOneAndUpdate(context.TODO(), filter, update).Decode(&account)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	vehicle.ID = id

	json.NewEncoder(w).Encode(vehicle)
}

func DeleteVehicle(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var params = mux.Vars(r)
	id, err := primitive.ObjectIDFromHex(params["id"])

	collection := helper.SelectCollection("vehicles")

	filter := bson.M{"_id": id}

	deleteResult, err := collection.DeleteOne(context.TODO(), filter)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	json.NewEncoder(w).Encode(deleteResult)
}

