package helper

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var DBCluster = GetEnv("DB_CLUSTER")
var DBName = GetEnv("DB_NAME")
var Client = mongo.Client{}



func ConnectDB() {
	clientOptions := options.Client().ApplyURI(DBCluster)

	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	Client = *client
	fmt.Println("Connected to MongoDB!")
}

func SelectCollection(collectionName string) *mongo.Collection {
	collection := Client.Database(DBName).Collection(collectionName)
	return collection
}


// ErrorResponse : This is error model.
type ErrorResponse struct {
	StatusCode   int    `json:"status"`
	ErrorMessage string `json:"message"`
}

func GetError(err error, w http.ResponseWriter) {

	var response = ErrorResponse{
		ErrorMessage: err.Error(),
		StatusCode:   http.StatusInternalServerError,
	}
	if err != nil {
		log.Fatal(err.Error())
	}
	message, _ := json.Marshal(response)
	w.WriteHeader(response.StatusCode)
	_, _ = w.Write(message)
}