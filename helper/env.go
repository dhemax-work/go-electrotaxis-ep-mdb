package helper

import (
	"github.com/joho/godotenv"
	"log"
	"os"
)

var loadEnv = godotenv.Load(".env")

func GetEnv(key string) string {
	if loadEnv != nil {
		log.Fatalf("Error loading .env file")
		return "nil"
	}

	return os.Getenv(key)
}

func SetEnv(key string, value string) bool {
	if loadEnv != nil {
		return false
	}

	os.Setenv(key, value)
	return true
}
