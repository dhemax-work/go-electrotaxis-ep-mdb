package main

import (
	accounts "./controllers/accounts"
	books "./controllers/books"
	locations "./controllers/locations"
	persons "./controllers/persons"
	travels "./controllers/travels"
	vehicles "./controllers/vehicles"
	helper "./helper"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func main(){
	helper.ConnectDB()
	r := mux.NewRouter()

	r.HandleFunc("/api/books", books.GetBooks).Methods("GET")
	r.HandleFunc("/api/books/{id}", books.GetBook).Methods("GET")
	r.HandleFunc("/api/books", books.CreateBook).Methods("POST")
	r.HandleFunc("/api/books/{id}", books.UpdateBook).Methods("PUT")
	r.HandleFunc("/api/books/{id}", books.DeleteBook).Methods("DELETE")

	r.HandleFunc("/api/locations", locations.GetLocations).Methods("GET")
	r.HandleFunc("/api/locations/{id}", locations.GetLocation).Methods("GET")
	r.HandleFunc("/api/locations", locations.CreateLocation).Methods("POST")
	r.HandleFunc("/api/locations/{id}", locations.UpdateLocation).Methods("PUT")
	r.HandleFunc("/api/locations/{id}", locations.DeleteLocation).Methods("DELETE")

	r.HandleFunc("/api/accounts", accounts.GetAccounts).Methods("GET")
	r.HandleFunc("/api/accounts/{id}", accounts.GetAccount).Methods("GET")
	r.HandleFunc("/api/accounts", accounts.CreateAccount).Methods("POST")
	r.HandleFunc("/api/accounts/{id}", accounts.UpdateAccount).Methods("PUT")
	r.HandleFunc("/api/accounts/{id}", accounts.DeleteAccount).Methods("DELETE")

	r.HandleFunc("/api/vehicles", vehicles.GetVehicles).Methods("GET")
	r.HandleFunc("/api/vehicles/{id}", vehicles.GetVehicle).Methods("GET")
	r.HandleFunc("/api/vehicles", vehicles.CreateVehicle).Methods("POST")
	r.HandleFunc("/api/vehicles/{id}", vehicles.UpdateVehicle).Methods("PUT")
	r.HandleFunc("/api/vehicles/{id}", vehicles.DeleteVehicle).Methods("DELETE")

	r.HandleFunc("/api/persons", persons.GetPersons).Methods("GET")
	r.HandleFunc("/api/persons/{id}", persons.GetPerson).Methods("GET")
	r.HandleFunc("/api/persons", persons.CreatePerson).Methods("POST")
	r.HandleFunc("/api/persons/{id}", persons.UpdatePerson).Methods("PUT")
	r.HandleFunc("/api/persons/{id}", persons.DeletePerson).Methods("DELETE")

	r.HandleFunc("/api/travels", travels.GetTravels).Methods("GET")
	r.HandleFunc("/api/travels/{id}", travels.GetTravel).Methods("GET")
	r.HandleFunc("/api/travels", travels.CreateTravel).Methods("POST")
	r.HandleFunc("/api/travels/{id}", travels.UpdateTravel).Methods("PUT")
	r.HandleFunc("/api/travels/{id}", travels.DeleteTravel).Methods("DELETE")


	log.Fatal(http.ListenAndServe(":8000", r))
}



