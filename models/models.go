package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Location struct{
	ID primitive.ObjectID 	`json:"_id,omitempty" bson:"_id,omitempty"`
	XPosition string 		`json:"xposition" bson:"xposition"`
	ZPosition string 		`json:"zposition" bson:"zposition"`
	Timestamp primitive.Timestamp	 `json:"updated" bson:"updated"`
}

type Account struct {
	ID primitive.ObjectID 	`json:"_id,omitempty" bson:"_id,omitempty"`
	Email string 			`json:"email" bson:"email"`
	Password string			`json:"password,omitempty" bson:"password,omitempty"`
	Rol string 				`json:"rol,omitempty" bson:"rol,omitempty"`
	PrUI string 			`json:"prUi,omitempty" bson:"prUi,omitempty"`
	Created primitive.Timestamp  `json:"created,omitempty" bson:"created,omitempty"`
}

type Vehicle struct {
	ID primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Patent string `json:"patent" bson:"patent"`
	Location *Location `json:"location" bson:"location"`
	AccountID primitive.ObjectID `json:"accountId" bson:"accountId"`
	Created primitive.Timestamp		 `json:"created" bson:"created"`
	Updated primitive.Timestamp `json:"updated,omitempty" bson:"updated,omitempty"`
}


type Person struct {
	ID primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	FirstName string `json:"firstname" bson:"firstname"`
	LastName  string `json:"lastname,omitempty" bson:"lastname,omitempty"`
	Avatar string `json:"avatar,omitempty" bson:"avatar,omitempty"`
	Location *Location `json:"location" bson:"location"`
}


type Travel struct {
	ID primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	VehicleID primitive.ObjectID `json:"vehicleId" bson:"vehicleId"`
	AccountID primitive.ObjectID `json:"accountId" bson:"accountId"`
	StartLocation *Location `json:"startLocation" bson:"startLocation"`
	EndLocation *Location `json:"endLocation" bson:"endLocation"`
	Status int `json:"status" bson:"status"`
	Created primitive.Timestamp		 `json:"created" bson:"created"`
	Updated primitive.Timestamp		 `json:"updated" bson:"updated"`
}